﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System;
using UnityEngine;
using UnityEngine.UI;

public class QED : MonoBehaviour
{
    public RenderTexture m_texture;
    public Transform m_barHost;
    public GameObject m_binBarPrefab;
    public Text m_progress;

    enum Mode {
        FULL_QED,
        SINGLE_PATH
    }
    
    enum TestVariant {
        CIRCLE_OCCLUDER = 0,
        BOXES = 1,
        DOUBLE_SLIT = 2
    }

    // Settings
    private float WAVELENGTHS_IN_WIDTH = 1.0f;
    private float HIGHLIGHT_SPOT_IGNORE_AREA = 20.0f;
    private Mode m_mode = Mode.FULL_QED;
    private TestVariant m_testVariant = TestVariant.CIRCLE_OCCLUDER;
    private bool m_outputToPngFiles = false;
    private bool m_onlyEdge = false;

    private static int SIZE = 128; // width and height of the test area/texture, in pixels
    private Texture2D m_texture2d;
    private XnaGeometry.Vector3[] m_accumVectors = new XnaGeometry.Vector3[SIZE*SIZE];
    private bool[] m_lit = new bool[SIZE*SIZE];
    private Color[] m_colors = new Color[SIZE*SIZE];
    private XnaGeometry.Vector2 m_lightPos = new XnaGeometry.Vector2(3, SIZE/2);
    private double m_brightness = 0.45;
    private RectTransform[] m_bars = new RectTransform[SIZE];
    private double m_pathSegmentLength = 1.0;

    // Start is called before the first frame update
    void Start()
    {
        // Prepare texture and "paint" walls with blue color
        m_texture2d = new Texture2D(SIZE, SIZE, TextureFormat.RGBA32, false);
        CreateTestScene(m_testVariant);
        CreateBars();
        UpdateTexture(m_colors);
    }
    
    private void CreateBars() {
        float ySize = 1024.0f / SIZE;
        for(int i = 0; i < SIZE; ++i) {
            GameObject go = Instantiate(m_binBarPrefab, m_barHost);
            go.GetComponent<RectTransform>().localPosition = new Vector3(-140, i * -ySize, 0);
            go.GetComponent<RectTransform>().sizeDelta = new Vector2(0, ySize - 2);
            m_bars[i] = go.GetComponent<RectTransform>();
        }
    }
    
    private void CreateTestScene(TestVariant testVariant) {
        XnaGeometry.Vector2 v = new XnaGeometry.Vector2();
        for(int y = 0; y < SIZE; ++y) {
            for(int x = 0; x < SIZE; ++x) {
                m_lit[y * SIZE + x] = true;
                m_accumVectors[y * SIZE + x] = new XnaGeometry.Vector3(0, 0, 0);
                m_colors[y * SIZE + x] = new Color(0, 0, 0);
            }
        }
        switch(testVariant) {
            case TestVariant.CIRCLE_OCCLUDER:
                for(int y = 0; y < SIZE; ++y) {
                    for(int x = 0; x < SIZE; ++x) {
                        v.X = (double) x - SIZE/2; 
                        v.Y = (double) y - SIZE/2; 
                        double d = FromCenter(v);
                        if(d < SIZE/8) {
                            m_colors[y * SIZE + x] = new Color(0, 0, 1);
                            m_lit[y * SIZE + x] = false;
                        }
                    }
                }
                break;
            case TestVariant.BOXES:
                for(int y = 0; y < SIZE; ++y) {
                    for(int x = 0; x < SIZE; ++x) {
                        if(((Math.Abs(x - SIZE/4) < SIZE/16) && (Math.Abs(y - SIZE/4) < SIZE/16)) || 
                           ((Math.Abs(x - SIZE*3/4) < SIZE/16) && (Math.Abs(y - SIZE/4) < SIZE/16)) ||
                           ((Math.Abs(x - SIZE/4) < SIZE/16) && (Math.Abs(y - SIZE*3/4) < SIZE/16)) ||
                           ((Math.Abs(x - SIZE*3/4) < SIZE/16) && (Math.Abs(y - SIZE*3/4) < SIZE/16))) {
                            m_colors[y * SIZE + x] = new Color(0, 0, 1);
                            m_lit[y * SIZE + x] = false;
                        }
                    }
                }
                break;
            case TestVariant.DOUBLE_SLIT:
                for(int y = 0; y < SIZE; ++y) {
                    for(int x = SIZE/2-2; x < SIZE/2+2; ++x) {
                        if(!((Math.Abs(y - SIZE/3) < SIZE/32) || 
                             (Math.Abs(y - SIZE*2/3) < SIZE/32))) {
                            m_colors[y * SIZE + x] = new Color(0, 0, 1);
                            m_lit[y * SIZE + x] = false;
                        }
                    }
                }
                break;
        }
        ClearBuffer();
    }

    private void UpdateTexture(Color[] colors) {
        m_texture2d.SetPixels(colors);
        m_texture2d.Apply();
        Graphics.CopyTexture(m_texture2d, m_texture);
        // Update bars to indicate right edge intensities
        for(int i = 0; i < SIZE; ++i) {
            float intensity = colors[(SIZE-1-i) * SIZE + SIZE - 1].r;
            m_bars[i].sizeDelta = new Vector2(intensity * 140.0f, m_bars[i].sizeDelta.y);
        }
    }

    private double FromCenter(XnaGeometry.Vector2 v) {
        return Mathf.Sqrt((float) (v.X * v.X + v.Y * v.Y));
    }

    void VectorsForSegment(XnaGeometry.Vector2 from, XnaGeometry.Vector2 to, ref XnaGeometry.Vector2 forward, ref XnaGeometry.Vector2 side, float randomness) {
        forward = to - from;
        side.X = -forward.Y * randomness;
        side.Y = forward.X * randomness;
    }

    bool InsideAWall(XnaGeometry.Vector2 v) {
        if((v.X >= 0) && (v.X < SIZE) && (v.Y >= 0) && (v.Y < SIZE)) {
            int idx = ((int) v.Y) * SIZE + ((int) v.X);
            return !m_lit[idx];
        }
        return true;
    }

    /*
     * This is the core function in terms of the QED simulation. 
     * This function should return representative random paths and their lengths.
     * If there's a bias here, it will manifest itself as artifacts in the image and produce a false end result.
     */
    double RandomPath(XnaGeometry.Vector2 from, XnaGeometry.Vector2 to, ref bool ok, float randomness, bool isSingleMode) {
        XnaGeometry.Vector2 forward = new XnaGeometry.Vector2(0, 0);
        XnaGeometry.Vector2 side = new XnaGeometry.Vector2(0, 0);
        VectorsForSegment(from, to, ref forward, ref side, randomness);
        double l = forward.LengthSquared();

        // check for recursion end condition:
        // if we are less than configured path segment length away, return length
        if(l <= m_pathSegmentLength) {
            return  (double) Mathf.Sqrt((float)l);
        }

        // choose "random" middle point
        XnaGeometry.Vector2 middle = from + forward * 0.5;
        middle += side * UnityEngine.Random.Range(-1.0f, 1.0f);

        // check that new middle point is not inside a wall
        if(InsideAWall(middle)) {
            ok = false;
            return 0;
        }

        if(isSingleMode) {
            // If we are visualizing single path, place "light spot" at each path point to make it visible
            DepositPhotonAt(middle, 1.0, 0.0);
        }

        // handle new sub-paths recursively
        double retLength = RandomPath(middle, to, ref ok, randomness, isSingleMode);
        if(!ok) {
            return 0;
        }
        retLength += RandomPath(from, middle, ref ok, randomness, isSingleMode);
        if(!ok) {
            return 0;
        }

        // all ok, returning sum of sub-path lengths
        return retLength;
    }

    void DepositPhotonAt(XnaGeometry.Vector2 target, double len, double baseFreq) {
        double freq = baseFreq + len * ((2.0f * Mathf.PI) / (double) SIZE) * WAVELENGTHS_IN_WIDTH;
        int idx = ((int) target.Y) * SIZE + ((int) target.X);
        m_accumVectors[idx].X += Mathf.Cos((float)freq);
        m_accumVectors[idx].Y += Mathf.Sin((float)freq);
    }

    bool FollowOnePath(double baseFreq, int x, int y, bool isSingleMode, ref bool findAnotherPath) {
        XnaGeometry.Vector2 target = new XnaGeometry.Vector2(x, y);

        // Add some sub-pixel offset for smoother results
        target.X += UnityEngine.Random.Range(0.0f, 1.0f);
        target.Y += UnityEngine.Random.Range(0.0f, 1.0f);

        // Check target for out of bounds
        if(InsideAWall(target)) {
            return false;
        }
        bool ok = true;
        float randomness = UnityEngine.Random.Range(0.0f, 1.0f);
        m_pathSegmentLength = UnityEngine.Random.Range(0.5f, 8.0f);
        double len = RandomPath(m_lightPos, target, ref ok, randomness, isSingleMode);
        if(!ok) {
            if(isSingleMode) {
                ClearBuffer();
            }
            findAnotherPath = true;
            return false;
        } else {
            DepositPhotonAt(target, len, baseFreq);
            findAnotherPath = false;
        }
        return true;
    }

    // Let's make the app more responsive: calculate only 1/10 lines on every frame
    private int baseline = 0;
    void AccumulatePaths() {
        int startCol = 0;
        int endCol = SIZE;
        if(m_onlyEdge) {
            startCol = SIZE-1;
            endCol = SIZE;
        }
        
        for(int line = (baseline % 10); line < SIZE; line += 10) {
            for(int col = startCol; col < endCol; ++col) {
                bool findAnotherPath = false;
                if(!FollowOnePath(0, col, line, false, ref findAnotherPath)) {
                    if(findAnotherPath) {
                        // try the same position again
                        --col;
                    }
                    continue;
                }
            }
        }
        ++baseline;
    }

    void ShowSingleRandomPath() {
        bool findAnotherPath = true;
        int triesLeft = 100;
        while(findAnotherPath) {
            FollowOnePath(0, (int) (110.0 / 128.0 * SIZE), (int) (64.0 / 128.0 * SIZE), true, ref findAnotherPath);
            if(--triesLeft <= 0) {
                break;
            }
            if(findAnotherPath) {
                ClearBuffer();
            }
        }
    }

    void ClearBuffer() {
        for(int y = 0; y < SIZE; ++y) {
            for(int x = 0; x < SIZE; ++x) {
                int idx = y * SIZE + x;
                m_accumVectors[idx].X = 0;
                m_accumVectors[idx].Y = 0;
            }
        }
        baseline = 0;
    }

    void ShowQEDImage() {
        // Find max intensity (ignoring the lightpos surroundings, since we might have an extremely bright spot there)
        double maxP = 0.0;
        XnaGeometry.Vector2 v = new XnaGeometry.Vector2(0, 0);
        for(int y = 0; y < SIZE; ++y) {
            for(int x = 0; x < SIZE; ++x) {
                XnaGeometry.Vector2 mp = new XnaGeometry.Vector2(x - SIZE/2, y - SIZE/2);
                if((mp - m_lightPos).Length() > HIGHLIGHT_SPOT_IGNORE_AREA) {
                    int idx = y * SIZE + x;
                    if(m_lit[idx]) {
                        v.X = m_accumVectors[idx].X;
                        v.Y = m_accumVectors[idx].Y;
                        double prob = Mathf.Pow((float)v.Length(), 2.0f);
                        if(prob > maxP) {
                            maxP = prob;
                        }
                    }
                }
            }
        }

        if(m_mode == Mode.SINGLE_PATH) {
            // for the single path mode, brighten up the visualization a bit
            maxP *= 0.3;
        } else {
            maxP *= (0.45 / m_brightness);
        }

        // Update texture colors, mapping vectors into brightness, clamping max intensity to pure white
        for(int y = 0; y < SIZE; ++y) {
            for(int x = 0; x < SIZE; ++x) {
                int idx = y * SIZE + x;
                if(m_lit[idx]) {
                    v.X = m_accumVectors[idx].X;
                    v.Y = m_accumVectors[idx].Y;
                    double prob = Mathf.Pow((float)v.Length(), 2.0f);
                    double c = Mathf.Min(1.0f, (float) (prob / maxP));
                    m_colors[idx].r = (float)c;
                    m_colors[idx].g = (float)c;
                    m_colors[idx].b = (float)c;
                }
            }
        }

        // indicate light location with a red dot
        int idx2 = ((int) m_lightPos.Y) * SIZE + ((int) m_lightPos.X);
        m_colors[idx2].r = 1.0f;
        m_colors[idx2].g = 0.0f;
        m_colors[idx2].b = 0.0f;

        UpdateTexture(m_colors);
    }

    private int frame = 0;
    
    // VERSION 3: Follow "fractal" paths from light source to all points in the scene
    void Update()
    {
        switch(m_mode) {
            case Mode.FULL_QED:
                AccumulatePaths();
                break;
            case Mode.SINGLE_PATH:
                ShowSingleRandomPath();
                break;
        }

        ShowQEDImage();

        if(m_mode == Mode.SINGLE_PATH) {
            ClearBuffer();
        }
        
        if(m_outputToPngFiles && ((baseline % 10) == 0)) {
            SaveFrameToPNGFile(frame++);
        }
        
        m_progress.text = "Paths per pixel: " + ((int) baseline/10);
    }
    
    void SaveFrameToPNGFile(int frameNo) {
        byte[] bytes = m_texture2d.EncodeToPNG();
        System.IO.File.WriteAllBytes("output/frame_" + frameNo + ".png", bytes);
    }

    // 
    // Control functions (called by the user interface)
    //

    public void SetScene(int scene) {
        m_testVariant = (TestVariant) scene;
        CreateTestScene(m_testVariant);
    }

    public void SetMode(bool singleMode) {
        m_mode = singleMode ? Mode.SINGLE_PATH : Mode.FULL_QED;
    }

    public void SetOnlyEdge(bool onlyEdge) {
        ClearBuffer();
        m_onlyEdge = onlyEdge;
    }

    public void SetWidth(float widthInNanometers) {
        ClearBuffer();
        WAVELENGTHS_IN_WIDTH = widthInNanometers / 500.0f;
        // Update related input field as well
        InputField inputField = GameObject.Find("WidthInputField").GetComponent<InputField>();
        inputField.text = "" + widthInNanometers;
    }

    public void SetBrightness(float brightness) {
        m_brightness = brightness;
        InputField inputField = GameObject.Find("BrightnessInputField").GetComponent<InputField>();
        inputField.text = "" + brightness;
    }

    public void BrightnessChanging(InputField brightness) {
        // if value can be parsed into a number, use that 
        try {
            float b = float.Parse(brightness.text, CultureInfo.InvariantCulture);
            if(b > 0.001f) {
                m_brightness = b;
            }
        } catch(FormatException e) {}
    }

    public void WidthChanging(InputField width) {
        // if value can be parsed into a number, use that 
        try {
            float w = float.Parse(width.text, CultureInfo.InvariantCulture);
            if(w > 0.001f) {
                ClearBuffer();
                WAVELENGTHS_IN_WIDTH = w / 500.0f;
            }
        } catch(FormatException e) {}
    }
}
